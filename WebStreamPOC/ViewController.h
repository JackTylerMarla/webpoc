//
//  ViewController.h
//  WebStreamPOC
//
//  Created by Emilio Lopez Martinez on 4/22/20.
//  Copyright © 2020 Papyrus Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (strong, nonatomic) WKWebView *webViewProgramatic;
@end

