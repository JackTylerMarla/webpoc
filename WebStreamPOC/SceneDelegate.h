//
//  SceneDelegate.h
//  WebStreamPOC
//
//  Created by Emilio Lopez Martinez on 4/22/20.
//  Copyright © 2020 Papyrus Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

