//
//  ViewController.m
//  WebStreamPOC
//
//  Created by Emilio Lopez Martinez on 4/22/20.
//  Copyright © 2020 Papyrus Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://n7bvxm2x.videomarketingplatform.co/papyrus-digital-open-house-1/8c151a6b53b6db03ffc1"]];
    
    //NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.google.com"]];

    //[self.webView loadRequest:request];
    
    WKWebViewConfiguration *config = [WKWebViewConfiguration new];
    self.webViewProgramatic = [[WKWebView alloc] initWithFrame:self.webView.frame configuration:config];
    [self.view addSubview:self.webViewProgramatic];
    self.webView.hidden = YES;
    [self.webViewProgramatic loadRequest:request];
}


@end
